insert into todo(id, username, description, target_date, is_done)
values(10001, 'DocMc', 'Learn JPA', sysdate(), false);

insert into todo(id, username, description, target_date, is_done)
values(10002, 'DocMc', 'Learn to dance', sysdate(), true);

insert into todo(id, username, description, target_date, is_done)
values(10003, 'DocMc', 'Go to Canada', sysdate(), false);

